import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Emergencia } from 'src/app/interfaces/emergencia.interface';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  license: Emergencia;
  public licencias: Emergencia[] = [];
  selectedMessage: any;
  columns: any[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.license = JSON.parse(params['license']);
      console.log('Info de detalle: ', this.license);
    });
    this.licencias.push(this.license);
    console.log('Licencias: ', this.licencias);
    this.columns = [
      { headerName: 'Nombre', field: 'name' },
      { headerName: 'Apellido', field: 'surname' },
      { headerName: 'ID HR', field: 'tenant' },
      { headerName: 'CUIL', field: 'cuil' },
      { headerName: 'Licencia', field: 'typeLicense' },
      { headerName: 'Diagnostico', field: 'medicDiagnostic' },
      { headerName: 'Fecha de certificado', field: 'medicalCertificateDate' },
      { headerName: 'Unidad Organizativa', field: 'workstation' },
      { headerName: 'Cargo', field: 'rol' },
      { headerName: 'Fecha de Inicio', field: 'initDate' },
      { headerName: 'Fecha de Fin', field: 'endingDate' }
    ];
  }

  actionDemo(event: any) {}
  getDemo(event: any) {}
  actionModal(event: any) {}

}
