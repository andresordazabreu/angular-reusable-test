import { Component, OnInit, EventEmitter } from '@angular/core';

import { EmergenciasService } from '../../services/emergencias.service';
import { Emergencia } from '../../interfaces/emergencia.interface';


@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css']
})
export class MasterComponent implements OnInit {
  public licencias: Emergencia[] = [];
  columns: any[];

  constructor(private emergenciaService: EmergenciasService) { }

  ngOnInit(): void {
    this.emergenciaService.fetchData().subscribe(data => {
      this.licencias = data.content;
    });

    this.columns = [
      { headerName: 'Id Solicitud', field: 'id', sortable: 'true' },
      { headerName: 'Nombre', field: 'name', sortable: 'true'  },
      { headerName: 'Apellido', field: 'surname' },
      { headerName: 'ID HR', field: 'tenant' },
      { headerName: 'CUIL', field: 'cuil' },
      { headerName: 'Licencia', field: 'typeLicense' },
      { headerName: 'Unidad Organizativa', field: 'workstation' },
      { headerName: 'Cargo', field: 'rol' , sortable: 'true'  },
      { headerName: 'Fecha de Inicio', field: 'initDate' },
      { headerName: 'Fecha de Fin', field: 'endingDate' },
      { headerName: 'Estado', field: 'status', sortable: 'true'   }
    ];
  }

  actionDemo(event: any) {}
  getDemo(event: any) {}
  actionModal(event: any) {}

}
