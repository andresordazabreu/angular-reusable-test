import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './license/detail/detail.component';
import { MasterComponent } from './license/master/master.component';

const routes: Routes = [
  { path: '', component: MasterComponent},
  { path: 'license', component: DetailComponent  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
