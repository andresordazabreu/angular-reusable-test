export interface JobSector {
  id: number;
  created: Date;
  updated: Date;
  code: string;
  description: string;
}
