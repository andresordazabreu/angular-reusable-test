import { JobSector } from './job-sector.interface';
import { LicenseTypeGroup } from './license-type-group.interface';
import { MedicDiagnostic } from './medic-diagnostic.interface';
import { OrganizationalUnit } from './organizational-unit.interface';
import { TypeLicense } from './type-license.interface';
import { User } from './user.interface';

export interface Emergencia {
    id: number;
    typeLicense: TypeLicense;
    days: number;
    initDate: Date;
    medicDiagnostic: MedicDiagnostic;
    acceptanceStatement: boolean;
    assignationType: string;
    medicCenter: string;
    approvedDays: number;
    codeOfEmployed: string;
    endingDate: Date;
    medicalCertificateDate: Date;
    birthDate: Date;
    neonatology: boolean;
    neonatologyDays: string;
    multipleBirth: boolean;
    numberOfchildren: number;
    probableBirthDate: string;
    fitToWork: boolean;
    jobSector: JobSector;
    gcbaLicenseStatus: string;
    organizationUnits: Array<OrganizationalUnit>;
    licenseTypesGroup: LicenseTypeGroup;
    medicalDecisions: Array<string>;
    user: User;
    appointmentDate: Date;
    observations: string;
}
