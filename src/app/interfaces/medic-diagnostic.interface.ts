export interface MedicDiagnostic {
  id: string;
  key: string;
  description: string;
  category: string;
  ter: number;
  autoDecision: string;
}
