import { JobPosition } from './job-position.interface';

export interface OrganizationalUnit {
    id: number;
    created: Date;
    updated: Date;
    codeOU: string;
    descriptionOU: string;
    jobPositions: Array<JobPosition>;
}
