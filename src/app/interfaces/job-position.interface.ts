export interface JobPosition {
  id: number;
  created: Date;
  updated: Date;
  jobPositionCode: string;
  jobPositionDescription: string;
  jobPositionNumericCode: number;
}
