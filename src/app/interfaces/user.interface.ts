export interface User {
  id: number;
  businessId: string;
  tenant: string;
  name: string;
  surname: string;
  cuil: string;
}
