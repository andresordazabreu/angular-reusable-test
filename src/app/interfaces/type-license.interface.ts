export interface TypeLicense {
  id: number;
  key: string;
  description: string;
  type: string;
  relationship: string;
}
