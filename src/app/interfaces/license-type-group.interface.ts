export interface LicenseTypeGroup {
  id: number;
  description: string;
}
