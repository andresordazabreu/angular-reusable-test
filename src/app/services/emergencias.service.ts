import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmergenciasService {
  private API_KEY = environment.emergencia.API_KEY;
  private url = environment.emergencia.AUTH_DOMAIN;

  constructor(private readonly http: HttpClient) { }

  public fetchData(): Observable<any> {
    const header = new HttpHeaders({ 'secret-key': this.API_KEY });
    const options = { headers: header };
    return this.http.get(this.url, options);
  }
}
