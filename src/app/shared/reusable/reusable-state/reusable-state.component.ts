import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-reusable-state',
  templateUrl: './reusable-state.component.html',
  styleUrls: ['./reusable-state.component.css']
})
export class ReusableStateComponent implements OnInit {

  @Input() status: any[]  = [];

  constructor() { }

  ngOnInit(): void {
    console.log('El estado es: ', status);

  }

}
