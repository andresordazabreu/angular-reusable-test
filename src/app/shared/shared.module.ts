import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReusableTableComponent } from './reusable/reusable-table/reusable-table.component';
import { ReusableToolbarComponent } from './reusable/reusable-toolbar/reusable-toolbar.component';
import { ReusableStateComponent } from './reusable/reusable-state/reusable-state.component';

@NgModule({
  declarations: [
    ReusableTableComponent,
    ReusableToolbarComponent,
    ReusableStateComponent],
  imports: [
    CommonModule
  ],
  exports: [
    ReusableTableComponent,
    ReusableToolbarComponent,
    ReusableStateComponent
  ]
})
export class SharedModule { }
