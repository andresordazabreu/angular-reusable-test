// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  emergencia: {
    API_KEY: '$2b$10$yjKVdPW/nMoCoGyJNONYW.nN6zePdd1I4wUZGKN4WGUJKDe0LVd7y',
    AUTH_DOMAIN: 'https://api.jsonbin.io/b/5f0887eb5d4af74b0129dd77'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
